import java.util.*;

public class DateUtils {
    private int MAX_DATES = 10;
    private MyDate dateList[] = new MyDate[MAX_DATES];
    int num_dates = 0;

    public DateUtils(MyDate[] dateList) {
        for (int i = 0; i < dateList.length; i++)
            this.dateList[i] = dateList[i];
        num_dates = dateList.length;
    }

    public int compare(MyDate date1, MyDate date2) {
        if (date1.getYear() > date2.getYear()) {
            return 1;
        }

        if (date2.getYear() > date1.getYear()) {
            return -1;
        }

        if (date1.getMonth() > date2.getMonth()) {
            return 1;
        }

        if (date2.getMonth() > date1.getMonth()) {
            return -1;
        }

        if (date1.getDay() > date2.getDay()) {
            return 1;
        }

        if (date2.getDay() > date1.getDay()) {
            return -1;
        }

        return 0;
    }

    public void sort() {
        ArrayList<MyDate> dates = new ArrayList<MyDate>();
        for (int i = 0; i < num_dates; i++)
            dates.add(dateList[i]);

        Collections.sort(dates, (a, b) -> {
            return compare(a, b);
        });

        System.out.println("Sorted list of date: ");
        for (MyDate date : dates)
            date.print();
    }
}
