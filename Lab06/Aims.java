import java.util.*;

public class Aims {

    public static void showMenu() {
        System.out.println();
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    static Order anOrder = null;

    public static void handleRequest(Scanner scanner) {
        showMenu();
        int choice = Integer.parseInt(scanner.nextLine());

        switch (choice) {
            case 1:
                anOrder = new Order();
                System.out.println("Created an Order!");
                break;
            case 2:
                if (anOrder == null) {
                    System.out.println("No order present! Please create one!");
                    break;
                }

                System.out.println("Choose the type of item you want to add (1: Book, 2: Disc)");
                int type = scanner.nextInt();
                scanner.nextLine();
                if (type == 1) {
                    System.out.println("Enter the title of the book: ");
                    String title = scanner.nextLine();

                    System.out.println("Enter the category of the book: ");
                    String category = scanner.nextLine();

                    System.out.println("How many authors does this book have?: ");
                    int numAuthor = Integer.parseInt(scanner.nextLine());
                    List<String> authors = new ArrayList<String>();

                    for (int i = 0; i < numAuthor; i++) {
                        System.out.println("Enter an author: ");
                        String authorName = scanner.nextLine();
                        authors.add(authorName);
                    }
                    Book aBook = new Book(title, category, authors);
                    aBook.setID(anOrder.getItemsOrdered().size());
                    anOrder.addMedia(aBook);
                } else {
                    System.out.println("Enter the title of the disc: ");
                    String title = scanner.nextLine();
                    System.out.println("Enter the category of the disc: ");
                    String category = scanner.nextLine();
                    System.out.println("Enter the director of the disc: ");
                    String director = scanner.nextLine();
                    System.out.println("Enter the length of the disc: ");
                    float length = scanner.nextFloat();
                    scanner.nextLine();

                    DigitalVideoDisc aDisc = new DigitalVideoDisc(title, category, director, length);
                    aDisc.setID(anOrder.getItemsOrdered().size());
                    anOrder.addMedia(aDisc);
                }
                break;
            case 3:
                if (anOrder == null) {
                    System.out.println("No order present! Please create one!");
                    break;
                }
                System.out.println("Enter item's ID: ");
                int id = Integer.parseInt(scanner.nextLine());
                if (id >= anOrder.getItemsOrdered().size() || id < 0) {
                    System.out.println("There's no item with this id!");
                    break;
                }
                anOrder.removeMediaByID(id);
                break;

            case 4:
                if (anOrder == null) {
                    System.out.println("No order present! Please create one!");
                    break;
                }

                for (Media cur : anOrder.getItemsOrdered()) {

                    System.out.println("Item number " + (cur.getID() + 1));
                    if (cur instanceof Book) {
                        System.out.println("The Book's title: " + cur.getTitle());
                        System.out.println("The Book's category: " + cur.getCategory());
                        System.out.println("And the authors are: ");
                        for (String name : ((Book) cur).getAuthors()) {
                            System.out.print(name + ", ");
                        }
                        System.out.println();
                    } else {
                        System.out.println("The Disc's title: " + cur.getTitle());
                        System.out.println("The Disc's category: " + cur.getCategory());
                        System.out.println("The Disc's director: " + ((DigitalVideoDisc) cur).getDirector());
                        System.out.println("The Disc's length: " + ((DigitalVideoDisc) cur).getLength());
                    }
                    System.out.println();
                }

                break;
            case 0:
                break;
            default:
                System.out.println("Invalid option! Try again!");
        }
        if (choice != 0)
            handleRequest(scanner);
        return;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        handleRequest(scanner);
        scanner.close();
    }
}
// Order anOrder = new Order();
// DigitalVideoDisc dvd1 = new DigitalVideoDisc("Harry Potter");
// dvd1.setCategory("Fantasy");
// dvd1.setCost(19.95f);
// dvd1.setDirector("Roger Allers");
// dvd1.setLength(87);
// System.out.println(dvd1.search("Potter Harry"));

// DigitalVideoDisc dvd2 = new DigitalVideoDisc("Big bang");
// dvd2.setCategory("Science");
// dvd2.setCost(255f);
// dvd2.setDirector("Jack Sollof");
// dvd2.setLength(100);

// DigitalVideoDisc dvd3 = new DigitalVideoDisc("How it happened");
// dvd3.setCategory("Science");
// dvd3.setCost(152f);
// dvd3.setDirector("Jonas Brothers");
// dvd3.setLength(100);

// DigitalVideoDisc[] dvdList = new DigitalVideoDisc[] { dvd1, dvd2 };
// anOrder.addDigitalVideoDisc(dvdList);

// System.out.println("You currently have " + anOrder.qtyOrdered + " dics in
// your order");
// System.out.println("Total cost is " + anOrder.totalCost());

// anOrder.removeDigitalVideoDisc(dvd1);
// System.out.println("You currently have " + anOrder.qtyOrdered + " dics in
// your order");
// System.out.println("Total cost after removing dvd1 is " +
// anOrder.totalCost());

// System.out.println();
// anOrder.printListOfOrder();

// Order anOrder2 = new Order();
// anOrder2.addDigitalVideoDisc(dvd3);
// anOrder2.addDigitalVideoDisc(dvd2);
// anOrder2.printListOfOrder();
// anOrder2.getALuckyItem();
// anOrder2.printListOfOrder();
