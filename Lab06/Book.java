import java.util.*;

public class Book extends Media {
    private List<String> authors = new ArrayList<String>();

    public Book() {
    }

    public Book(String title, String category, List<String> authors) {
        this.setTitle(title);
        this.setCategory(category);
        this.addAuthors(authors);
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void addAuthor(String authorName) {
        if (authors.contains(authorName))
            return;
        authors.add(authorName);
    }

    public void addAuthors(List<String> authors) {
        for (String cur : authors) {
            if (this.authors.contains(cur))
                continue;
            this.authors.add(cur);
        }
    }

    public void removeAuthor(String authorName) {
        if (!authors.contains(authorName))
            return;
        authors.remove(authorName);
    }
}
