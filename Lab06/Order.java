import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMITTED_ORDERS = 5;
    public static MyDate dateOrdered;
    public static int nbOrders;
    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();

    public ArrayList<Media> getItemsOrdered() {
        return itemsOrdered;
    }

    public Order() {
        if (nbOrders < MAX_LIMITTED_ORDERS) {
            nbOrders++;
            dateOrdered = new MyDate();
        } else
            System.out.println("You have reached the maximum number of orders!");
    }

    public void addMedia(Media cur) {
        if (itemsOrdered.contains(cur))
            return;
        itemsOrdered.add(cur);
    }

    public void removeMedia(Media cur) {
        if (!itemsOrdered.contains(cur))
            return;
        itemsOrdered.remove(cur);
    }

    public void removeMediaByID(int id) {
        itemsOrdered.remove(itemsOrdered.get(id));
    }

    // public void printListOfOrder() {
    // System.out.println("***********************Order***********************");
    // System.out.print("Date: ");
    // dateOrdered.print();
    // System.out.println("Ordered items:");
    // for (int i = 0; i < qtyOrdered; i++) {
    // System.out.println(i + 1 + ". DVD" + " - " + itemsOrdered[i].getTitle() + " -
    // "
    // + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector() + " -
    // "
    // + itemsOrdered[i].getLength() + ": " + itemsOrdered[i].getCost() + "$");
    // }
    // System.out.println("Total cost: " + totalCost(freeIndex) + "$");

    // System.out.println("***************************************************");
    // }

    public boolean isFull() {
        return (itemsOrdered.size() >= MAX_NUMBER_ORDERED) ? true : false;
    }

    public Media find(DigitalVideoDisc disc) {
        for (Media cur : itemsOrdered) {
            if (!disc.equals(cur))
                continue;

            return cur;
        }
        return null;
    }

    public float totalCost() {
        float cost = 0;
        for (Media cur : itemsOrdered) {
            cost += cur.getCost();
        }
        return cost;
    }
}
