import java.util.Scanner;

public class FindNumberOfDays {

    public static boolean checkLeap(int year)
    {
        if (year % 4 == 0)
        {
            if (year % 100 == 0 && year % 400 != 0)
                return false;
            return true;
        }
        return false;
    }

    public static int getNumDays(int year, int month)
    {
        if (month == 2)
            return (checkLeap(year)) ? 29 : 28;

        int checker = 1;
        if (month > 7)
            checker = 0;

        return (month % 2 == checker) ? 31 : 30;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year, month;

        do {
            System.out.println("Enter the year: ");
            year = scanner.nextInt();
            if (year <= 0)
                System.out.println("Invalid year, enter again!");
        }
        while(year <= 0);

        do {
            System.out.println("Enter the month (as a number): ");
            month = scanner.nextInt();
            if (month <= 0 || month > 12)
                System.out.println("Invalid month, enter again!");
        }
        while (month <= 0 || month > 12);

        System.out.println("This month has: " + getNumDays(year, month) + " days!");
    }
}
