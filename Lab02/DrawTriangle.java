import java.util.Scanner;

public class DrawTriangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the length of the triangle:");
        int n = scanner.nextInt();

        int maxLen = 1 + (n - 1) * 2;
        int mid = maxLen / 2;
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < maxLen; j++)
            {
                if (j <= mid + i && j >= mid - i)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }


    }
}
