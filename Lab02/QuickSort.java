import java.util.Scanner;

public class QuickSort {
    static void swap(int[] arr, int a, int b)
    {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }

    static int partition(int[] arr, int left, int right)
    {
        int pivot = arr[right];
        int pivotPosition = (left - 1);

        for(int j = left; j <= right - 1; j++)
        {
            if (arr[j] < pivot)
            {
                pivotPosition++;
                swap(arr, pivotPosition, j);
            }
        }
        swap(arr, pivotPosition + 1, right);
        return (pivotPosition + 1);
    }

    static void quickSort(int[] arr, int left, int right)
    {
        if (left < right)
        {
            int pi = partition(arr, left, right);

            quickSort(arr, left, pi - 1);
            quickSort(arr, pi + 1, right);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the length of the array you want to sort: ");
        int n = scanner.nextInt();
        int[] arr = new int[n];

        double sum = 0;
        System.out.println("Enter the elements of the array: ");
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
            sum += arr[i];
        }

        quickSort(arr, 0, n - 1);
        System.out.println("The sorted array now is: ");
        for (int i : arr)
            System.out.print(i + " ");
        System.out.println();

        double ave = sum / n;
        System.out.format("The sum of the array is: %.0f\n", sum);
        System.out.println("Ans its average value is: " + ave);
    }
}
