import java.util.Scanner;

public class AddTwoMatrices {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the size of both matrices(r c): ");
        int r = scanner.nextInt();
        int c = scanner.nextInt();

        int[][] m1 = new int[r][c];
        int[][] m2 = new int[r][c];


        System.out.println("Enter elements of the first matrix: ");
        for (int i = 0; i < r; i++)
        {
            for (int j = 0; j < c; j++)
                m1[i][j] = scanner.nextInt();
        }

        System.out.println("Enter elements of the second matrix: ");
        for (int i = 0; i < r; i++)
        {
            for (int j = 0; j < c; j++)
                m2[i][j] = scanner.nextInt();
        }

        System.out.println("The resultant matrix is: ");
        for (int i = 0; i < r; i++)
        {
            for (int j = 0; j < c; j++)
                System.out.print(m1[i][j] + m2[i][j] + " ");
            System.out.println();
        }
    }
}
