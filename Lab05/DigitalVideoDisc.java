import java.beans.ConstructorProperties;
import java.util.*;

public class DigitalVideoDisc {
    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    public DigitalVideoDisc(String title) {
        this.title = title;
    }

    public DigitalVideoDisc(String title, String category) {
        this.title = title;
        this.category = category;
    }

    public DigitalVideoDisc(String title, String category, String director) {
        this.title = title;
        this.category = category;
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        this.title = title;
        this.category = category;
        this.director = director;
        this.length = length;
        this.cost = cost;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getDirector() {
        return director;
    }

    public int getLength() {
        return length;
    }

    public float getCost() {
        return cost;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public boolean find(String can, String[] arr) {
        for (String i : arr) {
            if (i.equals(can))
                return true;
        }
        return false;
    }

    public boolean search(String title) {
        String[] com = title.split(" ");
        String[] comOfTitle = this.title.split(" ");
        for (int i = 0; i < comOfTitle.length; i++) {
            String temp = comOfTitle[i].replaceAll(" ", "");
            comOfTitle[i] = temp;
        }

        for (String i : com) {
            if (i.length() == 0)
                continue;
            if (!find(i, comOfTitle))
                return false;
        }
        return true;
    }
}
