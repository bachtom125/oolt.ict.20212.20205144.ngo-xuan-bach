import javax.swing.JOptionPane;
public class BasicMathOperations {
    public static double abs(double num)
    {
        return (num >= 0) ? num : num * (-1);
    }
    public static void main(String[] args) {
        String strNum1, strNum2;
        double num1, num2;
        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        num1 = Double.parseDouble(strNum1);
        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        num2 = Double.parseDouble(strNum2);
        
        String command;
        command = JOptionPane.showInputDialog(null, "Please input your choice of operation with the two numbers: \n (Sum, Difference, Product, Quotient)", "Input the operation", JOptionPane.INFORMATION_MESSAGE);
        
        switch(command)
        {
            case "Sum":
            {
                double sum = num1 + num2;
                String result = "The sum of the two numbers is: " + String.valueOf(sum);
                JOptionPane.showMessageDialog(null, result, "Calculate sum", JOptionPane.INFORMATION_MESSAGE);
                break;
            }

            case "Difference":
            {
                double dif = abs(num1 - num2);
                String result = "The difference of the two numbers is: " + String.valueOf(dif);
                JOptionPane.showMessageDialog(null, result, "Calculate difference", JOptionPane.INFORMATION_MESSAGE);
                break;
            }

            case "Product":
            {
                double pro = num1 * num2;
                String result = "The product of the two numbers is: " + String.valueOf(pro);
                JOptionPane.showMessageDialog(null, result, "Calculate product", JOptionPane.INFORMATION_MESSAGE);
                break;
            }

            case "Quotient":
            {
                double quotient = num1 / num2;
                if (quotient < 1)
                    quotient = 1 / quotient;

                String result = "The quotient of the two numbers is: " + String.valueOf(quotient);
                JOptionPane.showMessageDialog(null, result, "Calculate quotient", JOptionPane.INFORMATION_MESSAGE);
                break;
            }
        }
        System.exit(0);
    }
}
