import javax.swing.event.SwingPropertyChangeSupport;

import java.util.zip.InflaterOutputStream;

import javax.swing.JOptionPane;
import java.lang.Math;
public class EquationSolver {
    public static void linearEquation(double a, double b, double c)
    {
        String result = "The solution is: " + String.valueOf((c - b) / a);
        JOptionPane.showMessageDialog(null, result, "Calculate solution", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void linearSystem(double a1, double b1, double c1, double a2, double b2, double c2)
    {
        double D = a1 * b2 - a2 * b1;
        double D1 = c1 * b2 - c2 * b1;
        double D2 = a1 * c2 - a2 * c1;

        if (D != 0)
        {
            double x1 = D1/D;
            double x2 = D2/D;
            String result = "The solution is (x1, x2): (" + String.valueOf(x1) + ", " + String.valueOf(x2) + ")"; 
            JOptionPane.showMessageDialog(null, result, "Calculate solution", JOptionPane.INFORMATION_MESSAGE);
        }

        else if (D == 0 && D1 == 0 && D2 == 0)
        {
            String result = "The system has infinitely many solutions!";
            JOptionPane.showMessageDialog(null, result, "Calculate solution", JOptionPane.INFORMATION_MESSAGE);
        }

        else
        {
            JOptionPane.showMessageDialog(null, "The system has no solution!", "Calculate solution", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public static void secondDegree(double a, double b, double c)
    {
        double delta = b * b - 4 * a * c;
        if (delta > 0)
        {
            double r1 = (b * (-1) + Math.sqrt(delta)) / (2 * a);
            double r2 = (b * (-1) - Math.sqrt(delta)) / (2 * a);
            String result = "The system has two roots: " + String.valueOf(r1) + " and " + String.valueOf(r2);
            JOptionPane.showMessageDialog(null, result, "Calculate solution", JOptionPane.INFORMATION_MESSAGE);
        }
        else if (delta == 0)
        {
            double r = b * (-1) / (2 * a);
            String result = "The system has one root: " + String.valueOf(r);
            JOptionPane.showMessageDialog(null, result, "Calculate solution", JOptionPane.INFORMATION_MESSAGE);
        }
        else
        {
            String result = "The system has no solution!";
            JOptionPane.showMessageDialog(null, result, "Calculate solution", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public static void main(String[] args) {
        String command;
        command = JOptionPane.showInputDialog(null, "Please choose the type of equation you'd like solved! (Linear Equation, Linear System, Second Degree)", "Input type of equation", JOptionPane.INFORMATION_MESSAGE);
        switch(command)
        {
            case "Linear Equation":
            {
                double a, b, c;
                JOptionPane.showMessageDialog(null, "The equation has this form: ax + b = c", "Form of equation", JOptionPane.INFORMATION_MESSAGE);
                String sa = JOptionPane.showInputDialog(null, "Please Enter a: ", "Input a", JOptionPane.INFORMATION_MESSAGE);
                String sb = JOptionPane.showInputDialog(null, "Please Enter b: ", "Input b", JOptionPane.INFORMATION_MESSAGE);
                String sc = JOptionPane.showInputDialog(null, "Please Enter c: ", "Input c", JOptionPane.INFORMATION_MESSAGE);
                a = Double.parseDouble(sa);
                b = Double.parseDouble(sb);
                c = Double.parseDouble(sc);
                linearEquation(a, b, c);
                break;
            }

            case "Linear System":
            { 
                double a1, b1, c1, a2, b2, c2;
                JOptionPane.showMessageDialog(null, "The system has this form: \na1x1 + b1x2 = c1\na2x1 + b2x2 = c2", "Form of equation", JOptionPane.INFORMATION_MESSAGE);
                String sa1 = JOptionPane.showInputDialog(null, "Please Enter a1: ", "Input a1", JOptionPane.INFORMATION_MESSAGE);
                String sb1 = JOptionPane.showInputDialog(null, "Please Enter b1: ", "Input b1", JOptionPane.INFORMATION_MESSAGE);
                String sc1 = JOptionPane.showInputDialog(null, "Please Enter c1: ", "Input c1", JOptionPane.INFORMATION_MESSAGE);
                String sa2 = JOptionPane.showInputDialog(null, "Please Enter a2: ", "Input a2", JOptionPane.INFORMATION_MESSAGE);
                String sb2 = JOptionPane.showInputDialog(null, "Please Enter b2: ", "Input b2", JOptionPane.INFORMATION_MESSAGE);
                String sc2 = JOptionPane.showInputDialog(null, "Please Enter c2: ", "Input c2", JOptionPane.INFORMATION_MESSAGE);
                a1 = Double.parseDouble(sa1);
                b1 = Double.parseDouble(sb1);
                c1 = Double.parseDouble(sc1);
                a2 = Double.parseDouble(sa2);
                b2 = Double.parseDouble(sb2);
                c2 = Double.parseDouble(sc2);
                linearSystem(a1, b1, c1, a2, b2, c2);
                break;
            }

            case "Second Degree":
            {
                double a, b, c;
                JOptionPane.showMessageDialog(null, "The equation has this form: ax^2 + bx + c = 0", "Form of equation", JOptionPane.INFORMATION_MESSAGE);
                String sa = JOptionPane.showInputDialog(null, "Please Enter a: ", "Input a", JOptionPane.INFORMATION_MESSAGE);
                String sb = JOptionPane.showInputDialog(null, "Please Enter b: ", "Input b", JOptionPane.INFORMATION_MESSAGE);
                String sc = JOptionPane.showInputDialog(null, "Please Enter c: ", "Input c", JOptionPane.INFORMATION_MESSAGE);
                a = Double.parseDouble(sa);
                b = Double.parseDouble(sb);
                c = Double.parseDouble(sc);
                secondDegree(a, b, c);
                break;
            }

            default: 
            {
                JOptionPane.showMessageDialog(null, "Invalid option!", "Error", JOptionPane.INFORMATION_MESSAGE);
                break;
            }
        }
    }
}
