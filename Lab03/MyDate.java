import java.time.format.DateTimeFormatter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Arrays;

public class MyDate {
	private int day;
	private int month;
	private int year;

	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public MyDate() {
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
		SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
		Date today = new Date();
		this.day = Integer.parseInt(dayFormat.format(today));
		this.month = Integer.parseInt(monthFormat.format(today));
		this.year = Integer.parseInt(yearFormat.format(today));
	}

	public int findIndex(String[] months, String curMonth) {
		for (int i = 0; i < 12; i++) {
			if (months[i].equals(curMonth))
				return i;
		}
		return -1;
	}

	public MyDate(String date) {
		String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		String[] component = date.split(" ");
		this.day = Integer.parseInt(component[1].substring(0, 2));
		this.year = Integer.parseInt(component[2]);
		this.month = Arrays.asList(months).indexOf(component[0]) + 1;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public void print() {
		System.out.println(this.day + "-" + this.month + "-" + this.year);
	}
}
