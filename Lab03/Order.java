import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    public static int qtyOrdered;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];

    public boolean isFull()
    {
        return (qtyOrdered >= 10) ? true : false;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc)
    {
        if (isFull())
        {
            System.out.println("Your order is full!");
            System.out.println("Unable to complete order!");
            return;
        }
        qtyOrdered++;
        itemsOrdered[qtyOrdered - 1] = disc;
    }

    public int find(DigitalVideoDisc disc)
    {
        for (int i = 0; i < qtyOrdered; i++)
        {
            if (!disc.equals(itemsOrdered[i]))
                continue;

            return i;
        }
        return -1;
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc)
    {
        int index = find(disc);
        if (index < 0) {
            System.out.println("Unable to find such disc!");
            return;
        }

        qtyOrdered--;
        List <DigitalVideoDisc> afterDeleted = new ArrayList <DigitalVideoDisc>();
        Collections.addAll(afterDeleted, itemsOrdered);
        afterDeleted.remove(index);
        itemsOrdered = afterDeleted.toArray(new DigitalVideoDisc[qtyOrdered]);
        System.out.println("Deletion successful!");
    }

    public float totalCost()
    {
        float cost = 0;
        for (int i = 0; i < qtyOrdered; i++)
            cost += itemsOrdered[i].getCost();

        return cost;
    }
}
