public class Aims {
    public static void main(String[] args) {
        Order anOrder = new Order();
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("Harry Potter");
        dvd1.setCategory("Fantasy");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);

        anOrder.addDigitalVideoDisc(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Big bang");
        dvd2.setCategory("Science");
        dvd2.setCost(25f);
        dvd2.setDirector("Jack Sollof");
        dvd2.setLength(100);

        anOrder.addDigitalVideoDisc(dvd2);

        System.out.println("Total cost is " + anOrder.totalCost());

        anOrder.removeDigitalVideoDisc(dvd1);
        System.out.println("Total cost after removing dvd1 is " + anOrder.totalCost());
    }
}
