public class DateTest {
    public static void main(String[] args) {
        MyDate day1 = new MyDate("January 28th 2019"); // String parameter
        day1.print();

        MyDate day2 = new MyDate(); // No parameter
        day2.print();

        MyDate day3 = new MyDate(15, 5, 2022); // Int parameter
        day3.print();
    }
}
