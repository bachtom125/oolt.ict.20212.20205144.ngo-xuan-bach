
import java.nio.file.WatchEvent;
import java.sql.Wrapper;
import java.util.*;

import javax.print.attribute.standard.DialogTypeSelection;

public class TestPassingParameter {
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");

        ObjectWrapper WO1 = new ObjectWrapper(jungleDVD);
        ObjectWrapper WO2 = new ObjectWrapper(cinderellaDVD);
        swap(WO1, WO2);
        System.out.println("jungle dvd title after swapping: " + WO1.cur.getTitle());
        System.out.println("cinderella dvd title after swapping: " + WO2.cur.getTitle());
        changeTitle(jungleDVD, cinderellaDVD.getTitle());
        System.out.println("jungle dvd title: " + jungleDVD.getTitle());
    }

    public static void swap(ObjectWrapper WO1, ObjectWrapper WO2) {
        DigitalVideoDisc temp = WO1.cur;
        WO1.cur = WO2.cur;
        WO2.cur = temp;
    }

    public static void changeTitle(DigitalVideoDisc dvd, String title) {
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }

}
