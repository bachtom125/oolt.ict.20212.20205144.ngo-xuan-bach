import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMITTED_ORDERS = 5;
    public int qtyOrdered = 0;
    public static MyDate dateOrdered;
    public static int nbOrders;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBER_ORDERED];

    public Order() {
        if (nbOrders < MAX_LIMITTED_ORDERS) {
            nbOrders++;
            dateOrdered = new MyDate();
        } else
            System.out.println("You have reached the maximum number of orders!");
    }

    public void printListOfOrder() {
        System.out.println("***********************Order***********************");
        System.out.print("Date: ");
        dateOrdered.print();
        System.out.println("Ordered items:");
        for (int i = 0; i < qtyOrdered; i++) {
            System.out.println(i + 1 + ". DVD" + " - " + itemsOrdered[i].getTitle() + " - "
                    + itemsOrdered[i].getCategory() + " - " + itemsOrdered[i].getDirector() + " - "
                    + itemsOrdered[i].getLength() + ": " + itemsOrdered[i].getCost() + "$");
        }
        System.out.println("Total cost: " + totalCost() + "$");

        System.out.println("***************************************************");
    }

    public boolean isFull() {
        return (qtyOrdered >= MAX_NUMBER_ORDERED) ? true : false;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (isFull()) {
            System.out.println("Your order is full!");
            System.out.println("Unable to complete order!");
            return;
        }
        this.qtyOrdered++;
        itemsOrdered[qtyOrdered - 1] = disc;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
        if (qtyOrdered + dvdList.length > MAX_NUMBER_ORDERED)
            System.out.println("The remaining " + Math.abs(MAX_NUMBER_ORDERED - dvdList.length - qtyOrdered)
                    + " cannot be added as maximun capacity has been reached!");

        for (int i = 0; i + qtyOrdered + 1 <= MAX_NUMBER_ORDERED && i < dvdList.length; i++)
            addDigitalVideoDisc(dvdList[i]);
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
        if (qtyOrdered == MAX_NUMBER_ORDERED)
            System.out.println("Cannot add dvd titled: " + dvd1.getTitle());
        else
            addDigitalVideoDisc(dvd1);

        if (qtyOrdered == MAX_NUMBER_ORDERED)
            System.out.println("Cannot add dvd titled: " + dvd2.getTitle());
        else
            addDigitalVideoDisc(dvd2);
    }

    public int find(DigitalVideoDisc disc) {
        for (int i = 0; i < qtyOrdered; i++) {
            if (!disc.equals(itemsOrdered[i]))
                continue;

            return i;
        }
        return -1;
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        int index = find(disc);
        if (index < 0) {
            System.out.println("Unable to find such disc!");
            return;
        }

        qtyOrdered--;
        List<DigitalVideoDisc> afterDeleted = new ArrayList<DigitalVideoDisc>();
        Collections.addAll(afterDeleted, itemsOrdered);
        afterDeleted.remove(index);
        itemsOrdered = afterDeleted.toArray(new DigitalVideoDisc[qtyOrdered]);
        System.out.println("Deletion successful!");
    }

    public float totalCost() {
        float cost = 0;
        for (int i = 0; i < qtyOrdered; i++)
            cost += itemsOrdered[i].getCost();

        return cost;
    }
}
